﻿using TP01_Taboo_MAUI.Views;
using TP01_Taboo_MAUI.ViewModel;

namespace TP01_Taboo_MAUI;

public partial class AppShell : Shell
{
    public AppShell()
    {
        InitializeComponent();
        Application.Current.UserAppTheme = AppTheme.Light;
        Routing.RegisterRoute(nameof(HomePage), typeof(HomePage));
        Routing.RegisterRoute(nameof(PlayPage), typeof(PlayPage));
        Routing.RegisterRoute(nameof(ConfirmPage), typeof(ConfirmPage));
        Routing.RegisterRoute(nameof(EndPage), typeof(EndPage));
    }
}