﻿using TP01_Taboo_MAUI.ViewModel;

namespace TP01_Taboo_MAUI.Views;

public partial class HomePage : ContentPage 
{

    // Used for resteting the homePage value!!!
    // To be seen  
    // private string _previousTeam1Value;

    public HomePage()
    {
		InitializeComponent();
        BindingContext = new HomeViewModel();
    }

    void OnCliqued(object sender, EventArgs e)
    {
        Navigation.PushAsync(new PlayPage());
    }

}
