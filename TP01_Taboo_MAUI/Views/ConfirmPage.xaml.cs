﻿using TP01_Taboo_MAUI.ViewModel;

namespace TP01_Taboo_MAUI.Views;

public partial class ConfirmPage : ContentPage
{
	public ConfirmPage()
	{
		InitializeComponent();
        BindingContext = new ConfirmViewModel();
    }

    void OnCliqued(object sender, EventArgs e)
    {
        Navigation.PushAsync(new EndPage());
    }
}
