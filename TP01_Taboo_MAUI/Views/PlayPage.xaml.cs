﻿ using TP01_Taboo_MAUI.ViewModel;

namespace TP01_Taboo_MAUI.Views;

public partial class PlayPage : ContentPage
{
    
    

    public PlayPage()
    {
        InitializeComponent();
        BindingContext = new PlayViewModel();
    }

    void OnCliqued(object sender, EventArgs e)
    {
        Navigation.PushAsync(new ConfirmPage());
    }

    // To br used with Parameters
    // Allow to start the game with parameters loaded!!
    // Will be seen in the lecture
    protected override void OnNavigatedTo(NavigatedToEventArgs args)
    {
        base.OnNavigatedTo(args);
        
    }


}
