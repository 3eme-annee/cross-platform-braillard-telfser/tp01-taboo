﻿using TP01_Taboo_MAUI.ViewModel;

namespace TP01_Taboo_MAUI.Views;

public partial class EndPage : ContentPage
{

    public EndPage()
	{
        
        InitializeComponent();
        BindingContext = new EndViewModel();
	}

    protected override bool OnBackButtonPressed()
    {
        return true;
    }
}
