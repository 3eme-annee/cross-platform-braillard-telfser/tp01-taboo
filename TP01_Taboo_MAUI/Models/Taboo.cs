﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using CommunityToolkit.Mvvm.ComponentModel;

namespace TP01_Taboo_MAUI.Models
{
	public partial class Taboo : ObservableObject
	{
        [JsonProperty("word")]
        public string Word { get; set; }

        [JsonProperty("taboos")]
        public string[] Taboos { get; set; }


        [ObservableProperty]
        public CardState state; 

        [ObservableProperty]
        public ImageSource image;

        public Taboo(string word, string[] taboos)
        {
            this.Word = word;
            this.Taboos = taboos;
            State = CardState.FAILED;
        }
    }
}

