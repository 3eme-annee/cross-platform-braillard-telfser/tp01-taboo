﻿using Console = System.Console;
using IOException = System.IO.IOException;
using TP01_Taboo_MAUI.Interfaces;
using Newtonsoft.Json;
using CommunityToolkit.Mvvm.ComponentModel;
using System.Collections.ObjectModel;

namespace TP01_Taboo_MAUI.Models
{
	public partial class TabooManager : ObservableObject, IOnGameTimerEvent
    {

        // Interface used to listen to events related a game timer
        public interface IOnTabooUpdate
        {
            void OnTabooTick(int remainingTime, int progress);
            void OnTurnFinished();
        }

        // Default game parameters
        private static readonly bool DEFAULT_DIFFICULT = true;
        private static readonly int DEFAULT_NUMBER_OF_CARDS = 30;
        private static readonly int MAX_NUMBER_OF_CARDS = 56;
        private static readonly int DEFAULT_TURN_DURATION = 45;
        private static readonly int DEFAULT_NUMBER_OF_PASS = 2;
        private static readonly int DEFAULT_ERROR_PENALTY = 5;

        // Game Settings
        [ObservableProperty]
        private int numberOfCards;
        [ObservableProperty]
        private int turnDuration;
        [ObservableProperty]
        private int numberOfPass;
        [ObservableProperty]
        private int errorPenalty;
        [ObservableProperty]
        private List<Taboo> cards;

        private ObservableCollection<Taboo> playedThisRound;
        private List<Taboo> played;

        // Game info

        [ObservableProperty]
        private List<Team> teams;
        [ObservableProperty]
        private int successes;
        [ObservableProperty]
        private int failures;
        [ObservableProperty]
        private int availablePass;
        [ObservableProperty]
        private int currentTeam;
        [ObservableProperty]
        private Taboo currentCard;

        private GameTimer timer;
        private bool Playing;
        private bool gameOver;

        private static IOnTabooUpdate listener;

        private static TabooManager instance;  // unique instance

        private TabooManager() { } // constructor

        /**
         * Get the unique instance of the GameTimer class
         * @param listener used for timer updates
         * @return unique instance of GameTimer class
         */
        public static TabooManager GetInstance(IOnTabooUpdate listener)
        {
            if (instance == null)
            {
                instance = new TabooManager();
            }
            TabooManager.listener = listener;
            return instance;
        }

        /**
         * Constructor used to create a game with given parameters without listening to timer events
         *
         * @param isHard        True to set the game on easy. False to set it on hard
         * @param teams         Teams names playing the game
         * @param numberOfCards Number of cards in the game
         * @param turnDuration  Duration in seconds for each turn
         * @param numberOfPass  Number of pass that each team have at the begin of each turn
         * @param errorPenalty  Penalty time when making an error
         */
        public async Task SetupGame(bool isHard, List<String> teams, int numberOfCards, int turnDuration, int numberOfPass, int errorPenalty)
        {
            // Checking that the given parameters are valid. Otherwise, setting them to default values
            if (numberOfCards < 1 || numberOfCards > MAX_NUMBER_OF_CARDS)
                this.NumberOfCards = DEFAULT_NUMBER_OF_CARDS;
            else
                this.NumberOfCards = numberOfCards;

            if (turnDuration < 1)
                this.TurnDuration = DEFAULT_TURN_DURATION;
            else
                this.TurnDuration = turnDuration;

            if (numberOfPass < 0)
                this.NumberOfPass = DEFAULT_NUMBER_OF_PASS;
            else
                this.NumberOfPass = numberOfPass;

            if (errorPenalty < 0)
                this.ErrorPenalty = DEFAULT_ERROR_PENALTY;
            else
                this.ErrorPenalty = errorPenalty;

            this.Teams = new List<Team>();
            foreach (String name in teams)
            {
                this.Teams.Add(new Team(name));
            }

            Successes = 0;
            Failures = 0;
            AvailablePass = numberOfPass;
            CurrentTeam = 0;
            Playing = false;
            playedThisRound = new ObservableCollection<Taboo>();
            played = new List<Taboo>();
            gameOver = false;

            await LoadTaboosAsync(isHard);  // Loading taboo cards based on game difficult
        }

        /**
         * Start the current turn. A new countdown timer is then started
         */
        public void StartTurn()
        {
            if (!Playing)
            {
                NewCard();                     
                Playing = true;
                timer = GameTimer.GetInstance(this);
                timer.StartTimer(TurnDuration * GameTimer.SECOND, 10 * GameTimer.HUNDRED_MILLIS);
            }
        }

        /**
         * Validate the cards that have been played during the previous round
         */
        public void ValidateTurn()
        {
            Team team = Teams[CurrentTeam];
            while (playedThisRound.Count > 0)
            {
                Taboo card = playedThisRound[0];
                playedThisRound.RemoveAt(0);

                if (card.State == CardState.WON)
                {
                    team.IncrementScore();
                    played.Add(card);
                }
                else
                {
                    Cards.Add(card);
                }
            }

            if (played.Count < NumberOfCards)
            {
                NextTurn();
                StartTurn();
            }
            else
            {
                Teams.Sort( (o1, o2) => o2.score.CompareTo(o1.score));
                gameOver = true;
            }
        }

        /**
         * Call this method when the current playing team answers correctly to the current
         * taboo card
         **/
        public void Success()
        {
            if (Playing)
            {
                Successes++;
                CurrentCard.State = CardState.WON;
                NewCard();
            }
        }

        /**
         * Call this method when the current playing team answers wrongly to the current taboo card
         **/
        public void Failure()
        {
            if (Playing)
            {
                Failures++;
                CurrentCard.State = CardState.FAILED;
                timer.SubtractMillis(ErrorPenalty * GameTimer.SECOND);
                NewCard();
            }
        }

        /**
         * Call this method when the current playing team pass the current taboo card. This will
         * pick a new random taboo card
         **/
        public void Pass()
        {
            if (Playing)
            {
                if (--AvailablePass < 0)
                    AvailablePass = 0;
                else
                {
                    CurrentCard.State = CardState.PASSED;
                    NewCard();
                }
            }
        }

        /**
         * Resume the timer
         */
        public void ResumeTimer()
        {
            timer.ResumeTimer();
        }

        /**
         * Pause the timer
         */
        public void PauseTimer()
        {
            timer.PauseTimer();
        }

        /**
         * Stop the timer
         */
        public void StopTimer()
        {
            timer.StopTimer();
        }

        /**
         * @return The current team that is playing the current turn. The first team starts at
         * the value 0
         */
        public String GetCurrentTeam()
        {
            return Teams[CurrentTeam].name;
        }

        /**
         * @return The number of available pass for the current round
         */
        public int GetAvailablePass()
        {
            return AvailablePass;
        }

        /**
         * @return The number of correct answers given by the current playing team
         */
        public int GetCurrentSuccess()
        {
            return Successes;
        }

        /**
         * @return All the number of incorrect answers for each team. The index represent the
         * desired team
         */
        public int GetCurrentFailures()
        {
            return Failures;
        }

        /**
         * @return The duration in seconds for each turn
         */
        public int GetTurnDuration()
        {
            return TurnDuration;
        }

        /**
         * @return The current picked taboo card
         */
        public Taboo GetCard()
        {
            return CurrentCard;
        }

        /**
         * @return The list of teams
         */
        public List<Team> GetTeams()
        {
            return Teams;
        }

        /**
         * @return The number of remaining cards
         */
        public int GetNumberOfRemainingCards()
        {
            return Cards.Count;
        }

        /**
         * @return The number of pass
         */
        public int GetNumberOfPass()
        {
            return NumberOfPass;
        }

        /**
         * @return Cards that have been played this round
         */
        public ObservableCollection<Taboo> GetPlayedThisRound()
        {
            return playedThisRound;
        }

        /**
         * @return If the game is over
         */
        public bool IsGameOver()
        {
            return gameOver;
        }

        /**
         * @return If the game is playing
         */
        public bool IsPlaying()
        {
            return Playing;
        }

        /**
         * Pick a new random taboo card from the previously loaded list
         */
        private void NewCard()
        {
            if (Cards.Count != 0)
            {
                CurrentCard = Cards[0];
                Cards.RemoveAt(0);
                playedThisRound.Add(CurrentCard);
            }
            else
            {
                timer.StopTimer();
                Playing = false;
                listener.OnTurnFinished();
            }
        }

        /**
         * Prepare the next turn. If the game is finished or a turn is going on, this method
         * does nothing
         */
        private void NextTurn()
        {
            CurrentTeam = ++CurrentTeam % Teams.Count;
            AvailablePass = NumberOfPass;
            Successes = 0;
            Failures = 0;
        }

        /**
         * Load the list of taboo cards from a json file in the asset folder
         *
         * @param assetManager Used to retrieve the json asset containing taboo cards
         * @param isHard       Used to select the json file. True to load a set of hard taboo cards. False
         *                     for easy taboo cards
         */
        private async Task LoadTaboosAsync(bool isHard)
        {
            try
            {
                String fileName = "taboo_cards_" + (isHard ? "hard.json" : "easy.json");
                using Stream fileStream = await FileSystem.Current.OpenAppPackageFileAsync(fileName);
                using StreamReader reader = new StreamReader(fileStream);
                string jsonString = await reader.ReadToEndAsync();

                Cards = JsonConvert.DeserializeObject<List<Taboo>>(jsonString);

                while (Cards.Count() > NumberOfCards)
                    Cards.RemoveAt(NumberOfCards - 1);

                foreach (Taboo card in Cards) {
                    card.State = CardState.FAILED;   
                }

            } catch (IOException e) {
                Console.WriteLine("Load Taboo - JSON: " + e.Message );
            }
        }

        /**
         * Called on turn finished to set the state of currentCard to CardState.FAILED
         * because it stays CardState.PASSED if it was set on previous turn.
         * */
        public void OnTurnFinished()
        {
            if (CurrentCard.State == CardState.PASSED)
                CurrentCard.State = CardState.FAILED;
        }

        //****************************
        // Reset the game
        //****************************
        public void ResetGame()
        {
            NumberOfCards = 0;
            TurnDuration = 0;
            NumberOfPass = 0;
            ErrorPenalty = 0;
            Teams = null;
            OnNumberOfCardsChanged(NumberOfCards);
            OnNumberOfPassChanged(NumberOfPass);
            OnTurnDurationChanged(TurnDuration);
            OnErrorPenaltyChanged(ErrorPenalty);
            OnTeamsChanged(Teams);
        }


        /**
         * Called on each tick as defined in the second parameter of the method startTimer()
         * @param l remaining time
         */
        public void onTick(long l)
        {
            if (listener != null)
            {
                int time = (int)(l / GameTimer.SECOND);
                int progress = time * 100 / TurnDuration;
                listener.OnTabooTick(time, progress);
            }
        }

        /**
         * Method called when the countdown of the current round is finished and used to notify
         * the listener
         */
        public void onFinish()
        {
            Playing = false;

            listener?.OnTurnFinished();
        }
	}
}

