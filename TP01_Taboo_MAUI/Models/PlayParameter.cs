﻿using System;

namespace TP01_Taboo_MAUI.Models
{
	public class PlayParameter
	{
		public List<string> Teams { get; set; }
		public int Cards    { get; set; }
        public int Duration { get; set; }
        public int Pass     { get; set; }
        public int Penalty  { get; set; }
		public bool IsHard  { get; set; }
		public bool NewGame { get; set; }


        public PlayParameter(List<string> teams, int cards, int duration, int pass, int penalty, bool isHard, bool newGame)
		{
			Teams = teams;
			Cards = cards;
			Duration = duration;
			Pass = pass;
			Penalty = penalty;
			IsHard = isHard;
			NewGame = newGame;
		}

	}
}

