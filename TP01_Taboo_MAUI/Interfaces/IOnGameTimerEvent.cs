﻿using System;
namespace TP01_Taboo_MAUI.Interfaces
{
    /**
     * Interface to implement when using a GameTimer object
     */
    public interface IOnGameTimerEvent
    {

        /**
         * Called on each tick as defined in the second parameter of the method startTimer()
         * @param l remaining time
         */
        void onTick(long l);

        /**
         * Called once the countdown has finished
         */
        void onFinish();
    }
}

