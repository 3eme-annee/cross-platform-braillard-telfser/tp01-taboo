﻿
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Maui.Alerts;
using TP01_Taboo_MAUI.Views;
using TP01_Taboo_MAUI.Models;
using CommunityToolkit.Maui.Core;
using System.Diagnostics;

namespace TP01_Taboo_MAUI.ViewModel
{
    public partial class HomeViewModel: ObservableObject
    {

        // Observable object and properties
        [ObservableProperty]
        string team1;

        [ObservableProperty]
        string team2;

        [ObservableProperty]
        string team3;

        [ObservableProperty]
        string team4;

        [ObservableProperty]
        string cards;

        [ObservableProperty]
        string duration;

        [ObservableProperty]
        string pass;

        [ObservableProperty]
        string penalty;

        [ObservableProperty]
        bool isHardMode;


        //*******************************************

        public HomeViewModel()
        {
            // Do nothing
        }

        // Relay command to start the game...

        [RelayCommand]
        async void Play()
        {
            if (IsReadyToPlay().Result)
            {
                var teams = new List<String>();
                if (!string.IsNullOrEmpty(Team1)) teams.Add(Team1);
                if (!string.IsNullOrEmpty(Team2)) teams.Add(Team2);
                if (!string.IsNullOrEmpty(Team3)) teams.Add(Team3);
                if (!string.IsNullOrEmpty(Team4)) teams.Add(Team4);
                // Navigate to play page with params
                await Shell.Current.GoToAsync(nameof(PlayPage), new Dictionary<string, object>
                {
                    { "PlayParams", new PlayParameter(teams, int.Parse(Cards), int.Parse(Duration), int.Parse(Pass), int.Parse(Penalty), IsHardMode, true)}
                });
            }
        }

        private async Task<bool> IsReadyToPlay()
        {
            bool isValid = true;

            int numberOfTeams = 0;
            numberOfTeams += Team1 == "" ? 0 : 1;
            numberOfTeams += Team2 == "" ? 0 : 1;
            numberOfTeams += Team3 == "" ? 0 : 1;
            numberOfTeams += Team4 == "" ? 0 : 1;
            if (numberOfTeams < 2) isValid = false;

            if (!int.TryParse(Cards, out int cards)) isValid = false;
            if (cards < 0 || cards > 56) isValid = false;

            if (!int.TryParse(Duration, out int duration) || duration <= 0) isValid = false;

            if (!int.TryParse(Pass, out int pass) || pass < 0) isValid = false;

            if (!int.TryParse(Penalty, out int penalty) || penalty < 0) isValid = false;

            if (!isValid)
            {
                CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

                string text = "Game setup is invalide";
                ToastDuration toastDuration = ToastDuration.Short;
                double fontSize = 14;

                var toast = Toast.Make(text, toastDuration, fontSize);

                await toast.Show(cancellationTokenSource.Token);
            }

            return isValid;
        }
    }
    
}

