﻿using System;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using TP01_Taboo_MAUI.Models;
using System.Collections.ObjectModel;
using TP01_Taboo_MAUI.Views;

namespace TP01_Taboo_MAUI.ViewModel
{
    [QueryProperty(nameof(PlayParams), "PlayParams")]
    public partial class ConfirmViewModel : ObservableObject

    {
        private readonly TabooManager tabooManager;

        // Needed to indicate if it is a new game or not !!
        // Check how it is done in Android TP03 !!!
        [ObservableProperty]
        private PlayParameter playParams;
                
        [ObservableProperty]
        ObservableCollection<Taboo> playedTaboos;

        private bool isReady;

        public ConfirmViewModel()
        {
            isReady = false;

            tabooManager = TabooManager.GetInstance(null);

            playedTaboos = tabooManager.GetPlayedThisRound();

            foreach (var taboo in playedTaboos)
            {
                switch (taboo.State)
                {
                    case CardState.WON:
                        taboo.Image = "correct.png";
                        break;
                    case CardState.FAILED:
                        taboo.Image = "wrong.png";
                        break;
                    case CardState.PASSED:
                        taboo.Image = "passed.png";
                        break;
                }
            }
            
        }

        partial void OnPlayParamsChanged(PlayParameter value)
        {
            // Used to ensure that play params have been received
            isReady = true;
        }

        //*********************************
        // Commands:

        //*********************************
        // Change the state of the button
        // !!!! for instance ChangeState command become ChangeStateCommand by Mvvm toolkit !!!!
        // to put in xaml {Binding ChangeStateCommand}
        // [RelayCommand]...
        [RelayCommand]
        void ClickImage(string word)
        {
            Taboo currentTaboo = PlayedTaboos.SingleOrDefault(taboo => taboo.Word.Equals(word));

            if (currentTaboo == null) return;

            switch (currentTaboo.State)
            {
                case CardState.FAILED:
                    currentTaboo.State = CardState.PASSED;
                    currentTaboo.Image = "passed.png";
                    break;
                case CardState.PASSED:
                    currentTaboo.State = CardState.WON;
                    currentTaboo.Image = "correct.png";
                    break;
                case CardState.WON:
                    currentTaboo.State = CardState.FAILED;
                    currentTaboo.Image = "wrong.png";
                    break;
            }
        }

        //*******************
        // Validate command
        // ...
        [RelayCommand]
        async Task Validate()
        {
            tabooManager.ValidateTurn();

            if (!isReady) return;

            if (!tabooManager.IsGameOver())
            {
                PlayParams.NewGame = false;
                await Shell.Current.GoToAsync(nameof(PlayPage), new Dictionary<string, object>
                {
                    { "PlayParams", PlayParams }
                });
            } else
            {
                await Shell.Current.GoToAsync(nameof(EndPage));
            }
        }

    }
}

