﻿using System;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Collections;
using TP01_Taboo_MAUI.Models;
using TP01_Taboo_MAUI.Views;
using CommunityToolkit.Mvvm.Input;
using System.Collections.ObjectModel;

namespace TP01_Taboo_MAUI.ViewModel
{
    public partial class EndViewModel : ObservableObject
    {
        private TabooManager tabooManager;
        private List<Team> teams;

        [ObservableProperty]
        string nameTeam1;

        [ObservableProperty]
        int scoreTeam1;

        [ObservableProperty]
        ImageSource imageTeam1;

        [ObservableProperty]
        string nameTeam2;

        [ObservableProperty]
        int scoreTeam2;

        [ObservableProperty]
        ImageSource imageTeam2;

        [ObservableProperty]
        string nameTeam3;

        [ObservableProperty]
        int scoreTeam3;

        [ObservableProperty]
        ImageSource imageTeam3;

        [ObservableProperty]
        string nameTeam4;

        [ObservableProperty]
        int scoreTeam4;

        [ObservableProperty]
        ImageSource imageTeam4;

        public EndViewModel()
        {
            // Logic of the End View with the medal and scores
            tabooManager = TabooManager.GetInstance(null);
            teams = tabooManager.GetTeams();
            NameTeam1 = teams[0].name;
            ScoreTeam1 = teams[0].score;
            ImageTeam1 = "medal_gold.png";

            NameTeam2 = teams[1].name;
            ScoreTeam2 = teams[1].score;
            ImageTeam2 = "medal_silver.png";

            if (teams.Count < 3) return;

            NameTeam3 = teams[2].name;
            ScoreTeam3 = teams[2].score;
            ImageTeam3 = "medal_bronze.png";

            if (teams.Count < 4) return;

            NameTeam4 = teams[3].name;
            ScoreTeam4 = teams[3].score;
            ImageTeam4 = "medal_fail.png";
        }


        [RelayCommand]
        async Task ClickFinish()
        {
            await Shell.Current.GoToAsync(nameof(HomePage));
        }
    }
}

