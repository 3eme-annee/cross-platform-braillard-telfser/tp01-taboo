﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using TP01_Taboo_MAUI.Models;
using TP01_Taboo_MAUI.Views;

namespace TP01_Taboo_MAUI.ViewModel
{

    // set the QueryProperty to get the paameters 
    [QueryProperty(nameof(PlayParams), "PlayParams")]
    public partial class PlayViewModel : ObservableObject, TabooManager.IOnTabooUpdate
    {

        TabooManager tabooManager;

        // set the observable properties

        [ObservableProperty]
        PlayParameter playParams;

        [ObservableProperty]
        int remainingCards;

        [ObservableProperty]
        string currentTeam;

        [ObservableProperty]
        int timeLeft;

        [ObservableProperty]
        double timeProgress = 1d;

        [ObservableProperty]
        string word0;

        [ObservableProperty]
        string word1;

        [ObservableProperty]
        string word2;

        [ObservableProperty]
        string word3;

        [ObservableProperty]
        string word4;

        [ObservableProperty]
        int nbErrors;

        [ObservableProperty]
        int nbPasses;

        [ObservableProperty]
        int nbSuccesses;
        
        //------------------------------
        public PlayViewModel()
		{
            tabooManager = TabooManager.GetInstance(this);
        }

        partial void OnPlayParamsChanged(PlayParameter value)
        {
            StartTurn();
        }
        //--------------------------------------------
        public async void StartTurn()
        {

            if (PlayParams.NewGame)
            {
                await tabooManager.SetupGame(PlayParams.IsHard, PlayParams.Teams, PlayParams.Cards, PlayParams.Duration, PlayParams.Pass, PlayParams.Penalty);
            }

            // Start game
            tabooManager.StartTurn();

            // Update UI
            UpdateUI();

        }

        //**********************************************

        private void UpdateUI() {
            // Set ViewModel props with TabooManager props
            RemainingCards = tabooManager.GetNumberOfRemainingCards();
            CurrentTeam = tabooManager.GetCurrentTeam();
            NbErrors = tabooManager.GetCurrentFailures();
            NbPasses = tabooManager.GetAvailablePass();
            NbSuccesses = tabooManager.GetCurrentSuccess();

            // Set words from cards
            Word0 = tabooManager.CurrentCard.Word;
            Word1 = tabooManager.CurrentCard.Taboos[0];
            Word2 = tabooManager.CurrentCard.Taboos[1];
            Word3 = tabooManager.CurrentCard.Taboos[2];
            Word4 = tabooManager.CurrentCard.Taboos[3];
        }

        //**************************
        // *** 3 Buttons command ***
        //**************************

        [RelayCommand]
        void ClickError()
        {
            tabooManager.Failure();
            UpdateUI();
        }

        [RelayCommand]
        void ClickPass()
        {
            tabooManager.Pass();
            UpdateUI();
        }

        [RelayCommand]
        void ClickCorrect()
        {
            tabooManager.Success();
            UpdateUI();
        }


        //******************************************************
        // Interface implementation
        //******************************************************
        public void OnTabooTick(int remainingTime, int progress)
        {
            TimeLeft = remainingTime;
            TimeProgress = (double)progress / 100;
        }

        private async Task NavigateToConfirmPage()
        {
            await Shell.Current.GoToAsync(nameof(ConfirmPage), new Dictionary<string, object>
            {
                { "PlayParams", PlayParams}
            });
        }

        public void OnTurnFinished()
        {
            TimeProgress = 0;
            Console.WriteLine("Finished");
            PlayParams.Cards = tabooManager.GetNumberOfRemainingCards();
            PlayParams.Duration = tabooManager.GetTurnDuration();
            PlayParams.Pass = tabooManager.GetNumberOfPass();
            PlayParams.Penalty = tabooManager.ErrorPenalty;
            MainThread.InvokeOnMainThreadAsync(NavigateToConfirmPage);
        }

    }
}

